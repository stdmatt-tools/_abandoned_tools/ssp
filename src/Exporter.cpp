// Header
#include "../include/Exporter.h"
// std
#include <stdio.h>
// Json
#include "../libs/nlohmann/json.hpp"


//------------------------------------------------------------------------------
void
Exporter::SaveRectsToFile(ExportType type, std::string const &filename, std::vector<Graphics::Image> images)
{
    using json = nlohmann::json;
    json j;
    for(size_t i = 0; i < images.size(); ++i) {
        auto const &img = images[i];
        j["frames"][img.filename]["frame"] = { {"x", img.rect.x}, {"y", img.rect.y}, {"w", img.rect.w}, {"h", img.rect.h}};
    }
    
    auto a = j.dump(0);
    
    FILE *f = fopen(filename.c_str(), "w");
    fwrite(a.c_str(), sizeof(char), a.size(), f);
    fclose(f);
}
