//~---------------------------------------------------------------------------//
//                        __      __                  __   __                 //
//                .-----.|  |_.--|  |.--------.---.-.|  |_|  |_               //
//                |__ --||   _|  _  ||        |  _  ||   _|   _|              //
//                |_____||____|_____||__|__|__|___._||____|____|              //
//                                                                            //
//  File      : FS.cpp                                                        //
//  Project   : ssp                                                           //
//  Date      : Sep 15, 2019                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2019                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

// Header
#include "../include/FS.h"
// std
#include <string.h>
// Unix
#include <dirent.h>
#include <sys/stat.h>
// ssp
#include "../include/Utils.h"

//----------------------------------------------------------------------------//
// Internal Functions                                                         //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
bool
CheckStatMask(std::string const &path, uint32_t mask)
{
    struct stat sb = {0};
    if(stat(path.c_str(), &sb) != 0) {
        return false;
    }
    return (sb.st_mode & S_IFMT) == mask;
}

//----------------------------------------------------------------------------//
// Public Functions                                                           //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
bool
FS::IsFile(std::string const &path)
{
    return CheckStatMask(path, S_IFREG);
}

//------------------------------------------------------------------------------
bool
FS::IsDirectory(std::string const &path)
{
    return CheckStatMask(path, S_IFDIR);
}


//------------------------------------------------------------------------------
std::vector<std::string>
FS::GetFiles(std::string const &path, bool recursive)
{
    std::vector<std::string> entries;

    auto *p_DIR = opendir(path.c_str());
    if(!p_DIR) {
        return entries;
    }

    struct dirent *p_dirent = nullptr;
    while((p_dirent = readdir(p_DIR)) != nullptr) {
        auto p_name = p_dirent->d_name;
        if(strcmp(p_name, "..") == 0 || strcmp(p_name, ".") == 0) {
            continue;
        }

        // Make the fullname refers to the full path path of the entry.
        std::string fullname = path + "/" + p_name;

        bool is_dir = IsDirectory(fullname);
        if(recursive && is_dir) {
            auto recursive_entries = GetFiles(fullname, recursive);
            std::copy(
                std::begin(recursive_entries),
                std::end  (recursive_entries),
                std::back_inserter(entries)
            );
        } else {
            auto p_ext = fullname.c_str() + fullname.size() - 4;
            if(strcmp(p_ext, ".png") == 0 || strcmp(p_ext, ".jpg") == 0) {
                entries.push_back(fullname);
            }
        }
    }

    return entries;
}
