//~---------------------------------------------------------------------------//
//                        __      __                  __   __                 //
//                .-----.|  |_.--|  |.--------.---.-.|  |_|  |_               //
//                |__ --||   _|  _  ||        |  _  ||   _|   _|              //
//                |_____||____|_____||__|__|__|___._||____|____|              //
//                                                                            //
//  File      : Graphics.cpp                                                  //
//  Project   : ssp                                                           //
//  Date      : Sep 15, 2019                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2019                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

// Header
#include "../include/Graphics.h"
// std
#include <unordered_map>
// ssp
#include "../include/Utils.h"


//----------------------------------------------------------------------------//
// Private Vars                                                               //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
SDL_Window   *g_pWindow   = nullptr;
SDL_Renderer *g_pRenderer = nullptr;

std::unordered_map<uint32_t, SDL_Texture*> g_TexturesMap;
uint32_t g_TextureId = 0;


//----------------------------------------------------------------------------//
// Public Functions                                                           //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
void
Graphics::Initialize(Graphics::InitOptions const &options)
{
    // SDL
    VERIFY(
        SDL_Init(SDL_INIT_EVERYTHING) == 0,
        "Error initializing SDL - (%s)",
        SDL_GetError()
    );

    // SDL_Image
    const auto flags = (IMG_INIT_JPG | IMG_INIT_PNG);
    VERIFY(
        IMG_Init(flags) == flags,
        "Error initializing SDL_image - (%s)",
        IMG_GetError()
    );

    // Window and Renderer
    VERIFY(
        SDL_CreateWindowAndRenderer(
            options.windowWidth,
            options.windowHeight,
            SDL_WINDOW_SHOWN,
            &g_pWindow,
            &g_pRenderer
        ) == 0,
        "Failed to create SDL window and renderer - (%s)",
        SDL_GetError()
    );
}
//------------------------------------------------------------------------------
void
Graphics::Shutdown()
{

}


//------------------------------------------------------------------------------
void
Graphics::PresentTexture(SDL_Texture *pTexture)
{
    SDL_SetRenderDrawColor(g_pRenderer, 0, 0, 0, 0);
    SDL_RenderClear(g_pRenderer);
    SDL_RenderCopy(g_pRenderer, pTexture, nullptr, nullptr);
    SDL_RenderPresent(g_pRenderer);
}


//------------------------------------------------------------------------------
SDL_Texture*
Graphics::GenerateOutputTexture(
    std::vector<Image> const &images,
    uint32_t width,
    uint32_t height)
{
    // Create the texture.
    auto p_output_texture = SDL_CreateTexture(
        g_pRenderer,
        SDL_PIXELFORMAT_RGBA8888,
        SDL_TEXTUREACCESS_TARGET,
        width,
        height
    );
    VERIFY(
        p_output_texture,
        "Failed to create the screen texture - (%s)",
        SDL_GetError()
    );

    // Clear the texture.
    SDL_SetRenderTarget(g_pRenderer, p_output_texture);
//    SDL_SetRenderDrawBlendMode(g_pRenderer, SDL_BLENDMODE_BLEND);
//    SDL_SetRenderDrawColor(g_pRenderer, 0, 0, 255, 0);
//    SDL_RenderClear(g_pRenderer);

    // Render the images.
    for(auto const &image : images) {
        SDL_Rect src_rect;
        src_rect.x = 0;
        src_rect.y = 0;
        src_rect.w = image.rect.w;
        src_rect.h = image.rect.h;

        SDL_Rect dst_rect;
        dst_rect.x = image.rect.x;
        dst_rect.y = image.rect.y;
        dst_rect.w = image.rect.w;
        dst_rect.h = image.rect.h;

        SDL_Texture *p_img_texture = g_TexturesMap[image.textureId];
//        SDL_SetTextureColorMod(p_output_texture, 0, 0, 255);
        SDL_SetTextureBlendMode(p_img_texture, SDL_BLENDMODE_BLEND);
        SDL_RenderCopy(g_pRenderer, p_img_texture, &src_rect, &dst_rect);

        // SDL_SetRenderDrawColor(g_pRenderer, 255, 0, 255, 0xFF);
        // SDL_RenderDrawRect(g_pRenderer, &dst_rect);
    }

    SDL_SetRenderTarget(g_pRenderer, nullptr);
    return p_output_texture;
}

//------------------------------------------------------------------------------
void
Graphics::SaveTextureToFile(std::string const &filename, SDL_Texture *pTexture)
{
    SDL_SetRenderTarget(g_pRenderer, pTexture);

    int width, height;
    SDL_QueryTexture(pTexture, nullptr, nullptr, &width, &height);

    int bpp;
    uint32_t red_mask   = 0;
    uint32_t green_mask = 0;
    uint32_t blue_mask  = 0;
    uint32_t alpha_mask = 0;
    SDL_PixelFormatEnumToMasks(SDL_PIXELFORMAT_ARGB8888, &bpp, &red_mask, &green_mask, &blue_mask, &alpha_mask);
    
    auto p_surface = SDL_CreateRGBSurface(0, width, height, bpp, red_mask, green_mask, blue_mask, alpha_mask);
    SDL_RenderReadPixels(
        g_pRenderer,
        nullptr,
        SDL_PIXELFORMAT_ARGB8888,
        p_surface->pixels,
        p_surface->pitch
    );

    IMG_SavePNG(p_surface, filename.c_str());
    Graphics::SafeDestroySurface(p_surface);

    SDL_SetRenderTarget(g_pRenderer, nullptr);
}


//------------------------------------------------------------------------------
void
Graphics::SafeDestroySurface(SDL_Surface *&pSurface)
{
    if(pSurface) {
        SDL_FreeSurface(pSurface);
        pSurface = nullptr;
    }
}

//------------------------------------------------------------------------------
void
Graphics::SafeDestroyTexture(SDL_Texture *&pTexture)
{
    if(pTexture) {
        SDL_DestroyTexture(pTexture);
        pTexture = nullptr;
    }
}


//----------------------------------------------------------------------------//
// Image                                                                      //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
Graphics::Image
Graphics::LoadImageFromFile(std::string const &filename)
{
    auto p_surface = IMG_Load(filename.c_str());
    VERIFY(
        p_surface != nullptr,
        "Failed to load image - (%s)",
        filename.c_str()
    );

    auto p_texture = SDL_CreateTextureFromSurface(g_pRenderer, p_surface);
    VERIFY(
        p_texture != nullptr,
        "Failed create texture for image - (%s)",
        filename.c_str()
    );

    g_TexturesMap[g_TextureId] = p_texture;

    Graphics::Image img;
    img.filename  = filename;
    img.textureId = g_TextureId++;

    img.rect.x = 0;
    img.rect.y = 0;
    img.rect.w = p_surface->w;
    img.rect.h = p_surface->h;

    Graphics::SafeDestroySurface(p_surface);
    return img;
}

//------------------------------------------------------------------------------
void
Graphics::FreeImage(Graphics::Image &image)
{
    auto p_texture = g_TexturesMap[image.textureId];
    Graphics::SafeDestroyTexture(p_texture);
    g_TexturesMap.erase(image.textureId);
}
