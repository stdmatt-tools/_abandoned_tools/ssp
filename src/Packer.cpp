//~---------------------------------------------------------------------------//
//                        __      __                  __   __                 //
//                .-----.|  |_.--|  |.--------.---.-.|  |_|  |_               //
//                |__ --||   _|  _  ||        |  _  ||   _|   _|              //
//                |_____||____|_____||__|__|__|___._||____|____|              //
//                                                                            //
//  File      : Packer.cpp                                                    //
//  Project   : ssp                                                           //
//  Date      : Sep 15, 2019                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2019                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

// Header
#include "../include/Packer.h"
// ssp
#include "../include/Utils.h"

//----------------------------------------------------------------------------//
// Private Types                                                              //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
struct Node
{
    bool used = false;
    Rect rect;

    Node *p_right = nullptr;
    Node *p_down  = nullptr;
}; // struct Node

//----------------------------------------------------------------------------//
// Private Vars                                                               //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
Node            *g_pRootNode = nullptr;
Packer::Options *g_pOptions  = nullptr;

//----------------------------------------------------------------------------//
// Private Functions                                                          //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
static Node*
AllocNode(uint32_t x, uint32_t y, uint32_t w, uint32_t h)
{
    auto p_node = (Node*)malloc(sizeof(Node));
    memset(p_node, 0, sizeof(Node));
    p_node->rect.x = x;
    p_node->rect.y = y;
    p_node->rect.w = w;
    p_node->rect.h = h;

    return p_node;
}

//------------------------------------------------------------------------------
static void
FreeNode(Node *pNode)
{
    if(pNode) {
        FreeNode(pNode->p_right);
        FreeNode(pNode->p_down);
        SAFE_FREE(pNode);
    }
}

//------------------------------------------------------------------------------
static Node*
FindNode(Node* pNode, uint32_t w, uint32_t h)
{
    ASSERT(pNode, "pNode can't be nullptr");

    if(pNode->used) {
        auto p_found_node = FindNode(pNode->p_right, w, h);

        if(p_found_node) {
            return p_found_node;
        }

        return FindNode(pNode->p_down, w, h);
    } else if(w <= pNode->rect.w && h <= pNode->rect.h) {
        return pNode;
    }

    return nullptr;
}

//------------------------------------------------------------------------------
static Node*
SplitNode(Node* pNode, uint32_t w, uint32_t h)
{
    ASSERT(pNode, "pNode can't be nullptr");
    pNode->used = true;

    const auto &r = pNode->rect;
    pNode->p_down  = AllocNode(r.x    , r.y + h, r.w    , r.h - h);
    pNode->p_right = AllocNode(r.x + w, r.y    , r.w - w, h      );

    return pNode;
}

//------------------------------------------------------------------------------
static Node*
GrowNode_Right(uint32_t w, uint32_t h)
{
    auto const &r = g_pRootNode->rect;

    auto p_new_root = AllocNode(0, 0, r.w + w, r.h);

    p_new_root->used    = true;
    p_new_root->p_down  = g_pRootNode;
    p_new_root->p_right = AllocNode(r.w, 0, w, r.h);

    g_pRootNode = p_new_root;

    auto p_node = FindNode(g_pRootNode, w, h);
    if(p_node) {
        return SplitNode(p_node, w, h);
    }

    return nullptr;
}

//------------------------------------------------------------------------------
static Node*
GrowNode_Down(uint32_t w, uint32_t h)
{
    auto const &r = g_pRootNode->rect;

    auto p_new_root = AllocNode(0, 0, r.w, r.h + h);

    p_new_root->used    = true;
    p_new_root->p_down  = AllocNode(0, r.h, r.w, h);
    p_new_root->p_right = g_pRootNode;

    g_pRootNode = p_new_root;

    auto p_node = FindNode(g_pRootNode, w, h);
    if(p_node) {
        return SplitNode(p_node, w, h);
    }

    return nullptr;
}

//------------------------------------------------------------------------------
static Node*
GrowNode(uint32_t w, uint32_t h)
{
    // Just to reduce verbosity.
    auto const root_w = g_pRootNode->rect.w;
    auto const root_h = g_pRootNode->rect.h;

    auto can_grow_down  = (w <= root_w);
    auto can_grow_right = (h <= root_h);

    // Attempt to keep square-ish by growing right
    // when height is much greater than width
    auto should_grow_right = can_grow_right && (root_h >= (root_w + w));

    // Attempt to keep square-ish by growing down
    // when width is much greater than height
    auto should_grow_down = can_grow_down && (root_w >= (root_h + h));

    if     (should_grow_right) return GrowNode_Right(w, h);
    else if(should_grow_down ) return GrowNode_Down (w, h);
    else if(can_grow_right   ) return GrowNode_Right(w, h);
    else if(can_grow_down    ) return GrowNode_Down (w, h);
    else                       return nullptr;
}

//------------------------------------------------------------------------------
static void
SortImagesByWidth(std::vector<Graphics::Image> &images)
 {
     std::sort(
         std::begin(images),
         std::end  (images),
         [](Graphics::Image const &i1, Graphics::Image const i2) {
             return i1.rect.w > i2.rect.w;
         }
     );
 }

//----------------------------------------------------------------------------//
// Public Functions                                                           //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
Packer::SpriteSheet
Packer::Pack(std::vector<Graphics::Image> &images, Packer::Options const &options)
{
    SortImagesByWidth(images);

    auto const &bigger_img = images.front();
    auto const margin      = options.marging;

    g_pRootNode = AllocNode(
        0, 0,
        bigger_img.rect.w + margin,
        bigger_img.rect.h + margin
    );

    Packer::SpriteSheet ss = {};
    for(auto &img : images) {
        auto img_w = img.rect.w + margin;
        auto img_h = img.rect.h + margin;

        auto p_node = FindNode(g_pRootNode, img_w, img_h);
        if(!p_node) {
            p_node = GrowNode(img_w, img_h);
        } else {
            SplitNode(p_node, img_w, img_h);
        }

        img.rect.x = p_node->rect.x + margin;
        img.rect.y = p_node->rect.y + margin;

        ss.width  = std::max(ss.width,  img.rect.x + img.rect.w);
        ss.height = std::max(ss.height, img.rect.y + img.rect.h);
    }

    FreeNode(g_pRootNode);
    return ss;
}
